<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');

    $objek = new Animal("shaun");

    echo "Name : ". $objek->name . "<br>";
    echo "legs : ". $objek->legs. "<br>";
    echo "cold blooded : ". $objek->cold_blooded. "<br>";

    $objek3 = new Frog("buduk");
    echo "<br>Name : ". $objek3->name . "<br>";
    echo "legs : ". $objek3->legs. "<br>";
    echo "cold blooded : ". $objek3->cold_blooded. "<br>";
    echo $objek3->jump();

    $objek2 = new Ape("kera sakti");
    echo "<br>Name : ". $objek2->name . "<br>";
    echo "legs : ". $objek2->legs. "<br>";
    echo "cold blooded : ". $objek2->cold_blooded. "<br>";
    echo $objek2->yell(); 

?>