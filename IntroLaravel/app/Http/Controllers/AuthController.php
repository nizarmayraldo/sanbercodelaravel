<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pindah(){
        return view('register');
    }

    public function selesai(Request $request){
        // dd($request->all());
        $fname = $request['firstname'];
        $lname = $request['lastname'];
        return view('layout3', compact(['fname', 'lname']));
    }
}
