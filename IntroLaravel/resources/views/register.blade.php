<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/layout3" method="POST">
    @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name= "firstname"><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" id="lastname" name= "lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="Laki" id="Laki">Male<br>
        <input type="radio" name="Perempuan" id="Perempuan">Female<br>
        <input type="radio" name="Lain" id="Lain">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="WN" id="WN">
            <option value="Indo">Indoensia</option>
            <option value="Sing">Singaporean</option>
            <option value="Malay">Malaysian</option>
            <option value="Aussie">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="Biografi" id="Bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>